local telefrag = {}

local move = require "necro.game.system.Move"
local entities = require "system.game.Entities"
local collision = require "necro.game.tile.Collision"
local map = require "necro.game.object.Map"
local damage = require "necro.game.system.Damage"
local attack = require "necro.game.character.Attack"
local inventory = require "necro.game.item.Inventory"
local utils = require "system.utils.Utilities"
local minimapTheme = require "necro.game.data.tile.MinimapTheme"

function telefrag.teleport(teleporter, dest_x, dest_y)
    local targets = map.getAll(dest_x, dest_y)
    local targets2 = utils.shallowCopy(targets)
    for i,t in ipairs(targets) do
        local target = entities.getEntityByID(t)
        if target and attack.isAttackable(teleporter, target, attack.mask(attack.Flag.PHASING, attack.Flag.DIRECT)) then
                damage.inflict{
                    attacker = teleporter,
                    victim = target,
                    damage = 999,
                    penetration = damage.Penetration.PHASING
                }
        end
    end

    for i, t in ipairs(targets2) do
        local target = entities.getEntityByID(t)
        if target and target:hasComponent("item") and target:hasComponent("EnderDagger_enderDagger") then
            inventory.add(target, teleporter, true) --****
        end
    end
    local x,y = dest_x, dest_y
    if dest_x ~= teleporter.position.x or dest_y ~= teleporter.position.y then
        x,y = collision.findNearbyVacantTile(dest_x,  dest_y, collision.Group.SOLID)
    end
    move.absolute(teleporter, x, y, move.Type.NONE)

end

return telefrag