local event = require "necro.event.Event"
local telefrag = require "EnderDagger.utils.telefrag"
local collision = require "necro.game.tile.Collision"
local action = require "necro.game.system.Action"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"


components.register{
  enderDagger = {}
}

--Helper functions to find how far the dagger will travel
--Written by Pioneer, taken from Gunslinger mod and modified to only check for walls
--Finds distance to wall that the dagger is thrown
local function getDirectionOffset(direction)
  local dx, dy = action.getMovementOffset(direction)
  return (dx ~= 0) and dx / math.abs(dx) or 0, (dy ~= 0) and dy / math.abs(dy) or 0
end


local function checkReach(position, direction, maxDistance)
  local x, y = position.x, position.y
  local dx, dy = getDirectionOffset(direction)
  for i = 1, maxDistance, 1 do
      if collision.check(x + dx * i, y + dy * i, collision.Type.WALL) then
          return i - 1
      end
  end
  return maxDistance
end


event.weaponAttack.add("throwEnderDagger", {sequence = 1, order = "noDamage", filter = "EnderDagger_enderDagger"}, function(ev)
    if ev.pattern.forceAttack == true then
        local distance = checkReach(ev.attacker.position, ev.direction, 100)
        local dx, dy = action.getMovementOffset(ev.direction)
        
        telefrag.teleport(ev.attacker, ev.attacker.position.x + dx * distance, ev.attacker.position.y + dy * distance)
    end
end)



--event.weaponAttack.override("addSwipeTrailThrow", {sequence = 2}, function(func, ev)
--  --dbg(ev.attacker)
--  dbg("what?")
--  if ev.weapon:hasComponent("EnderDagger_enderDagger") then
--    if ev.throwDistance ~= nil then
--      local distance = checkReach(ev.attacker.position, ev.direction, 100)
--      for i = 1, distance - 1 do
--        ev.result.swipes[#ev.result.swipes+1] =  {
--          type = "trail",
--          data = {offset = - distance + i - 1},
--          weapon = ev.weapon 
--        }
--      end
--    end
--  else
--    func(ev)
--  end
--end)
--
customEntities.extend {
  name = "enderDagger",
  template = customEntities.template.item("weapon_dagger"),
  data = {
    flyaway = "Ender Dagger",
    hint = "Teleporting throws",
    slot = "weapon"
  },
  components = {
    sprite = {
      texture = "mods/EnderDagger/gfx/ender_dagger.png"
    },
    EnderDagger_enderDagger = {},
    itemPoolBlackChest = {
      weights = {2,2,2,2,2},
    },
    itemPoolShop = {
      weights = {25,25,25,25}
    },
    itemPoolEnchant = {
      weights = {1}
    },
    itemPrice = {
      coins = 120
    }
  }
}